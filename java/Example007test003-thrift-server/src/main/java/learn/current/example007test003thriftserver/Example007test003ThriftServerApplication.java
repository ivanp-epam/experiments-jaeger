package learn.current.example007test003thriftserver;

import example007.test003.thrift.Calculator;
import io.opentracing.Tracer;
import io.opentracing.contrib.concurrent.TracedExecutorService;
import io.opentracing.thrift.SpanProcessor;
import io.opentracing.thrift.SpanProtocol;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.Executors;

@SpringBootApplication
public class Example007test003ThriftServerApplication {

    @Value("${thrift.port}")
    private int port;
    private TServer server;
    @Autowired
    private Tracer tracer;
    @PostConstruct
    public void init() {
        try {
            TNonblockingServerSocket serverTransport = new TNonblockingServerSocket(port);
            TProcessor processor = new Calculator.Processor(this::sum);
            TThreadedSelectorServer.Args ragz = new TThreadedSelectorServer.Args(serverTransport)
                    .processor(new SpanProcessor(processor,tracer))
                    .protocolFactory(new TBinaryProtocol.Factory())
                    .selectorThreads(1)
                    .executorService(new TracedExecutorService(Executors.newFixedThreadPool(1),tracer));
            server = new TThreadedSelectorServer(ragz);
            System.out.println("Starting the NonBlockingThreadedServer server on port: " + port);
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long sum(long a, long b) {
        return a + b;
    }

    @PreDestroy
    public void destroy() {
        if (server != null) {
            server.stop();
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Example007test003ThriftServerApplication.class, args);
    }

}
