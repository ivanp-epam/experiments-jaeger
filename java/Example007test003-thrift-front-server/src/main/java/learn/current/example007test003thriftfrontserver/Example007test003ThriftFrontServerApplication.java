package learn.current.example007test003thriftfrontserver;

import example007.test003.thrift.Calculator;
import io.opentracing.Tracer;
import io.opentracing.thrift.SpanProtocol;
import org.apache.thrift.TConfiguration;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.layered.TFramedTransport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@RestController
@SpringBootApplication
public class Example007test003ThriftFrontServerApplication {
    @Value("${thrift.port}")
    private int port;
    @Value("${thrift.host}")
    private String host;
    @Autowired
    private Tracer tracer;

    public static void main(String[] args) {
        SpringApplication.run(Example007test003ThriftFrontServerApplication.class, args);
    }


    public Calculator.Client client() throws TTransportException {
        var calculatorFactory = new Calculator.Client.Factory();
        TTransport transport = new TFramedTransport(new TSocket(new TConfiguration(), host, port, 1000));
        transport.open();
        Calculator.Client calculator = calculatorFactory.getClient(new SpanProtocol(new TBinaryProtocol(transport),tracer));
        return calculator;
    }

    @GetMapping("calculator/sum/{a}/{b}")
    public  Long summarize(@PathVariable Long a, @PathVariable Long b) {
        try {
            return client().sum(a, b);
        } catch (TException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(new ApiInfoBuilder()
                        .title("Note API")
                        .description("A CRUD API to demonstrate Springfox 3 integration")
                        .version("0.0.1-SNAPSHOT")
                        .license("MIT")
                        .licenseUrl("https://opensource.org/licenses/MIT")
                        .build())
                .tags(new Tag("Note", "Endpoints for CRUD operations on notes"))
                .select().apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .build();
    }
}
