<?php


namespace App\Commands;


use Jaeger\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected static $defaultName = 'test1';

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//init server span start
        $config = Config::getInstance();

        $tracer = $config->initTracer('jaeger123', '127.0.0.1:6831');

        $top = $tracer->startActiveSpan('level top');
        $second = $tracer->startActiveSpan('level second');
        $third = $tracer->startActiveSpan('level third');

        $num = 0;
        for ($i = 0; $i < 10; $i++){
            $num += 1;
        }
        $third->getSpan()->setTag("num", $num);
        sleep(1);
        $third->close();

        $num = 0;
        for ($i = 0; $i < 10; $i++){
            $num += 2;
        }
        $third->getSpan()->setTag("num", $num);
        sleep(1);
        $second->close();


        $top->close();

//trace flush
        $config->flush();

        // ... put here the code to create the user

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }

}