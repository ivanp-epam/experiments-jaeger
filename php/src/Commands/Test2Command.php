<?php


namespace App\Commands;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use const Jaeger\SAMPLER_TYPE_CONST;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Jaeger\Config;
use OpenTracing\GlobalTracer;

class Test2Command extends Command
{
    protected static $defaultName = 'test2';

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = new Logger('logger',[new StreamHandler('php://stdout')]);
        $config = new Config(
            $this->getJaegerConfig(),
            'your-bunny-wrote',
            $logger
        );

        $config->initializeTracer();

        $tracer = GlobalTracer::get();

        $scope = $tracer->startActiveSpan('Start', []);

        // 1. указать ucp ID как тэг
        // 2. спан для каждого интеграционного вызова
        $scope->getSpan()->setTag("some-log", "value1");
        $scope->getSpan()->setTag("tag2", "value2");
        $scope->getSpan()->setTag("tag3", "value2");
        $scope->getSpan()->log([
            "key1" => "value1",
            "key2" => 2,
            "key3" => true
        ]);
        $scope->getSpan()->addBaggageItem("baggage-item1", "baggage-value1");
        $scope->getSpan()->addBaggageItem("baggage-item2", "baggage-value2");
        $scope->getSpan()->addBaggageItem("baggage-item3", "baggage-value3");

        $nestedSpanScope = $tracer->startActiveSpan("Nested1");
        $nestedSpanScope->getSpan()->setTag("tag5", "value5");
        $nestedSpanScope->getSpan()->setTag("tag6", "value6");
        $nestedSpanScope->getSpan()->setTag("tag7", "value7");
        $nestedSpanScope->getSpan()->log([
            "someKey" => "some_value",
            "key1" => "value1",
            "key2" => 2,
            "key3" => true
        ]);

        $nestedSpanScope->getSpan()->addBaggageItem("baggage-item4", "baggage-value4");
        $nestedSpanScope->getSpan()->addBaggageItem("baggage-item5", "baggage-value5");
        $nestedSpanScope->getSpan()->addBaggageItem("baggage-item6", "baggage-value6");

        sleep(1);

        $nestedSpanScope->close();

        sleep(1);
        $scope->close();
        $tracer->flush();

        // ... put here the code to create the user

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }

    private function getJaegerConfig(): array
    {
        return [
            'sampler' => [
                'type' => SAMPLER_TYPE_CONST,
                'param' => true,
            ],
            'logging' => true,
            "tags" => [
                // process. prefix works only with JAEGER_OVER_HTTP, JAEGER_OVER_BINARY
                // otherwise it will be shown as simple global tag
                "process.process-tag-key-1" => "process-value-1", // all tags with `process.` prefix goes to process section
                "process.process-tag-key-2" => "process-value-2", // all tags with `process.` prefix goes to process section
                "global-tag-key-1" => "global-tag-value-1", // this tag will be appended to all spans
                "global-tag-key-2" => "global-tag-value-2", // this tag will be appended to all spans
            ],
            "local_agent" => [
                "reporting_host" => "jaeger",
//        You can override port by setting local_agent.reporting_port value
                "reporting_port" => 6832
            ],
//     Different ways to send data to Jaeger. Config::ZIPKIN_OVER_COMPACT - default):
            'dispatch_mode' => Config::JAEGER_OVER_BINARY_UDP,
        ];
    }

}