<?php


namespace App\Commands;

use App\Opentracing\Thrift\SpanProtocol;
use App\Thrift\Example\CalculatorClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TFramedTransport;
use Thrift\Transport\TSocket;
use const Jaeger\SAMPLER_TYPE_CONST;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Jaeger\Config;
use OpenTracing\GlobalTracer;
use const OpenTracing\Tags\SPAN_KIND;

class ThriftCommand extends Command
{
    protected static $defaultName = 'thrift';

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->initTracer();
        $tracer = GlobalTracer::get();

        $socket = new TSocket('java-thrift-server', 9876);
        $transport = new TFramedTransport($socket, 1024, 1024);

        $protocol = new SpanProtocol(new TBinaryProtocol($transport), $tracer);
        $client = new CalculatorClient($protocol);

        $transport->open();
        $scope = $tracer->startActiveSpan('sum', []);

//        $nestedSpanScope = $tracer->startActiveSpan("Call the thrift");

        // thrift
        $val = $client->sum(1, 2);

//        $nestedSpanScope->close();

        var_dump($val);

        $scope->close();
        $tracer->flush();

        return Command::SUCCESS;
    }

    private function initTracer()
    {
        $config = new Config(
            $this->getJaegerConfig(),
            'your-bunny-wrote'
        );

        $config->initializeTracer();

    }

    private function getJaegerConfig(): array
    {
        return [
            'sampler' => [
                'type' => SAMPLER_TYPE_CONST,
                'param' => true,
            ],
            'logging' => true,
//            "tags" => [
//                // process. prefix works only with JAEGER_OVER_HTTP, JAEGER_OVER_BINARY
//                // otherwise it will be shown as simple global tag
//                "process.process-tag-key-1" => "process-value-1", // all tags with `process.` prefix goes to process section
//                "process.process-tag-key-2" => "process-value-2", // all tags with `process.` prefix goes to process section
//                "global-tag-key-1" => "global-tag-value-1", // this tag will be appended to all spans
//                "global-tag-key-2" => "global-tag-value-2", // this tag will be appended to all spans
//            ],
            "local_agent" => [
                "reporting_host" => "jaeger",
//        You can override port by setting local_agent.reporting_port value
                "reporting_port" => 6832
            ],
//     Different ways to send data to Jaeger. Config::ZIPKIN_OVER_COMPACT - default):
            'dispatch_mode' => Config::JAEGER_OVER_BINARY_UDP,
        ];
    }

}