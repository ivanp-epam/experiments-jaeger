<?php


namespace App\Opentracing\Thrift;

use OpenTracing\Span;
use Throwable;

/**
 * Default decorator for spans generated on the client side.
 */
class DefaultClientSpanDecorator implements ClientSpanDecorator
{

    public function decorate(Span $span, TMessage $message): void
    {
        SpanDecorator::decorate($span, $message);
    }

    public function onError(Throwable $throwable, Span $span): void
    {
        SpanDecorator::onError($throwable, $span);
    }
}