<?php


namespace App\Opentracing\Thrift;


class TMessage
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var int
     */
    public $seqid;

    /**
     * TMessage constructor.
     * @param string $name
     * @param string $type
     * @param int $seqid
     */
    public function __construct(string $name, string $type, int $seqid)
    {
        $this->name = $name;
        $this->type = $type;
        $this->seqid = $seqid;
    }
}