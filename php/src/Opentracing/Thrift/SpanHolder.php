<?php

namespace App\Opentracing\Thrift;

use OpenTracing\Span;
use Thrift\Protocol\TProtocolDecorator;

class SpanHolder
{
    /**
     * @var ?Span
     */
    private $span;

    /**
     * @return ?Span
     */
    public function getSpan(): ?Span
    {
        return $this->span;
    }

    /**
     * @param ?Span $span
     */
    public function setSpan(?Span $span): void
    {
        $this->span = $span;
    }

    public function clear(): void
    {
        $this->span = null;
    }

}