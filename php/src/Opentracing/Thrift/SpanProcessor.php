<?php


namespace App\Opentracing\Thrift;


use OpenTracing\Tracer;

class SpanProcessor
{
    public function __construct()
    {
        throw new \LogicException('Thrift Server Unsupported');
    }
}