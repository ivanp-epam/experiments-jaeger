<?php


namespace App\Opentracing\Thrift;


use OpenTracing\Tracer;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TProtocolDecorator;
use Thrift\Type\TMessageType;
use const OpenTracing\Tags\ERROR;

class ServerOutProtocolDecorator extends TProtocolDecorator
{
    /**
     * @var Tracer
     */
    private $tracer;

    protected function __construct(TProtocol $protocol,Tracer $tracer)
    {
        parent::__construct($protocol);
        $this->tracer = $tracer;
    }
    public function writeMessageBegin($name, $type, $seqid)
    {
    if ($this->tracer->activeSpan() !== null) {
        $this->tracer->activeSpan()->setTag(SpanDecorator::MESSAGE_TYPE, $type);
        if ($type === TMessageType::EXCEPTION) {
            $this->tracer->activeSpan()->setTag(ERROR, true);
        }
    }
    parent::writeMessageBegin($name, $type, $seqid);
    }
}