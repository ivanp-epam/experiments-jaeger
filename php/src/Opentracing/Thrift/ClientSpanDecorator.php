<?php


namespace App\Opentracing\Thrift;


use OpenTracing\Span;
use Throwable;

interface ClientSpanDecorator
{
    /**
     * Decorate a span with information from a TMessage.
     *
     * @param $span Span.
     * @param $message TMessage.
     */
    public function decorate(Span $span, TMessage $message):void;

    /**
     * Decorate a span with information from a caught throwable.
     *
     * @param $span Span.
     * @param $throwable Throwable.
     */
    public function onError(Throwable $throwable, Span $span):void;

}