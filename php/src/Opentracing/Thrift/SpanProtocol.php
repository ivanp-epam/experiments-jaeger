<?php

namespace App\Opentracing\Thrift;

use OpenTracing\GlobalTracer;
use OpenTracing\Tracer;
use Thrift\Exception\TTransportException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TProtocolDecorator;
use Thrift\Type\TMessageType;
use Thrift\Type\TType;
use const OpenTracing\Formats\TEXT_MAP;
use const OpenTracing\Tags\SPAN_KIND;
use const OpenTracing\Tags\SPAN_KIND_RPC_CLIENT;

/**
 * <code>SpanProtocol</code> is a protocol-independent concrete decorator that allows a Thrift
 * client to communicate with a tracing Thrift server, by adding the span context to the string
 * field during function calls.
 *
 * <p>NOTE: THIS IS NOT USED BY SERVERS.  On the server, use {@link SpanProcessor} to handle
 * requests from a tracing client.
 */
class SpanProtocol extends TProtocolDecorator
{
    /**
     * @var Tracer
     */
    private $tracer;

    /**
     * @var SpanHolder
     */
    private $spanHolder;

    /**
     * @var bool
     */
    private $finishSpan;
    /**
     * @var ClientSpanDecorator
     */
    private $spanDecorator;

    public const SPAN_FIELD_ID = 3333; // Magic number
    /**
     * @var bool
     */
    private $oneWay;
    /**
     * @var bool
     */
    private $injected;

    /**
     * Encloses the specified protocol. Take tracer from GlobalTracer
     *
     * @param TProtocol $protocol All operations will be forward to this protocol.
     * @param Tracer|null $tracer Tracer.
     * @param ClientSpanDecorator|null $spanDecorator
     * @param SpanHolder|null $spanHolder
     * @param bool $finishSpan
     */
    public function __construct(TProtocol $protocol, Tracer $tracer = null, ClientSpanDecorator $spanDecorator = null, SpanHolder $spanHolder = null, $finishSpan = true)
    {
        parent::__construct($protocol);
        if ($tracer) {
            $tracer = GlobalTracer::get();
        }
        /** @var Tracer $tracer */
        $this->tracer = $tracer;


        if (!$spanDecorator) {
            $spanDecorator = new DefaultClientSpanDecorator();
        }
        /** @var ClientSpanDecorator $spanDecorator */
        $this->spanDecorator = $spanDecorator;

        if (!$spanHolder) {
            $spanHolder = new SpanHolder();
        }
        /** @var SpanHolder $spanHolder */
        $this->spanHolder = $spanHolder;

        $this->finishSpan = $finishSpan;
    }

    public function writeMessageBegin($name, $type, $seqid)
    {
        $span = $this->tracer->startSpan($name);
        $span->setTag(SPAN_KIND, SPAN_KIND_RPC_CLIENT);

        $this->spanHolder->setSpan($span);

        $this->oneWay = ($type === TMessageType::ONEWAY);
        $this->injected = false;

        $this->spanDecorator->decorate($span, new TMessage($name, $type, $seqid));
        parent::writeMessageBegin($name, $type, $seqid);
    }

    public function writeMessageEnd()
    {
        try {
            parent::writeMessageEnd();
        } finally {
            $span = $this->spanHolder->getSpan();
            if ($span !== null && $this->oneWay && $this->finishSpan) {
                $span->finish();
                $this->spanHolder->setSpan(null);
            }
        }
    }

    public function writeFieldStop()
    {
        if (!$this->injected) {
            $span = $this->spanHolder->getSpan();
            if ($span !== null) {
                $map = [];
                $this->tracer->inject($span->getContext(), TEXT_MAP, $map);
                parent::writeFieldBegin("span", TType::MAP, self::SPAN_FIELD_ID);
                parent::writeMapBegin(TType::STRING, TType::STRING, count($map));

                foreach ($map as $key => $value) {
                    parent::writeString($key);
                    parent::writeString($value);

                }
                parent::writeMapEnd();
                parent::writeFieldEnd();
                $this->injected = true;
            }
        }

        parent::writeFieldStop();
    }

    public function readMessageBegin(&$name, &$type, &$seqid)
    {
        try {
            return parent::readMessageBegin($name, $type, $seqid);
        } catch (TTransportException $tte) {
            $span = $this->spanHolder->getSpan();
            if ($span !== null) {
                $this->spanDecorator->onError($tte, $span);
                if ($this->finishSpan) {
                    $span->finish();
                    $this->spanHolder->setSpan(null);
                }
            }
            throw $tte;
        }
    }

    public function readMessageEnd()
    {
        try {
            parent::readMessageEnd();
        } finally {
            $span = $this->spanHolder->getSpan();
            if ($span !== null && $this->finishSpan) {
                $span->finish();
                $this->spanHolder->setSpan(null);
            }
        }
    }
}