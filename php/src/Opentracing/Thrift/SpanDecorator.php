<?php


namespace App\Opentracing\Thrift;

use Jaeger\Span;
use Throwable;
use const OpenTracing\Tags\COMPONENT;
use const OpenTracing\Tags\ERROR;
use const OpenTracing\Tags\SPAN_KIND;

class SpanDecorator
{
    public const MESSAGE_TYPE = "message.type";
    public const COMPONENT_NAME = "php-thrift";

    public static function decorate(Span $span, TMessage $message): void
    {
        $span->setTag(SPAN_KIND, 'client');
        $span->setTag(COMPONENT, self::COMPONENT_NAME);
        $span->setTag('message.name', $message->name);
        $span->setTag(self::MESSAGE_TYPE, $message->type);
        $span->setTag('message.seqid', $message->seqid);
    }

    public static function onError(Throwable $throwable, Span $span): void
    {
        if ($span === null) {
            return;
        }
        $span->setTag(ERROR, true);
        $span->log(self::errorLogs($throwable));
    }

    private static function errorLogs(Throwable $throwable) // Map<String, Object>
    {
        $errorLogs = [
            "event" => ERROR
        ];

        if ($throwable !== null) {
            $errorLogs["error.object"] = $throwable;
        }
        return $errorLogs;
    }

}