namespace java example007.test003.thrift
namespace php App.Thrift.Example

service Calculator{
    i64 sum(1:i64 num1, 2:i64 num2);
}
